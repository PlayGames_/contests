package net.playgames_.contests;

public class ChatContest {
    
    public String text;
    public String answer;
    public int prize;
    public int amount;
    public boolean economy;
    
    public ChatContest(String text, String answer, int prize, int amount, boolean economy){
        this.text = text;
        this.answer = answer;
        this.prize = prize;
        this.amount = amount;
        this.economy = economy;
    }
    
    public String getAnswer(){
        return this.answer;
    }
    
    public String getQuestion(){
        return this.text;
    }
    
    public int getPrize(){
        return this.prize;
    }
    
    public int getAmount(){
        return this.amount;
    }
    
    public boolean getEconomy(){
        return this.economy;
    }

}
