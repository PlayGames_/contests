package net.playgames_.contests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

    public int contestAmount;
    public FileConfiguration config;
    public static ArrayList<ChatContest> contestQandA = new ArrayList<ChatContest>();
    public boolean contestActive;
    public ChatContest currentContest = new ChatContest("Question", "Answer", 1, 1, false);

    @Override
    public void onEnable() {
        System.out.println("-------------------------------------------------");
        System.out.println("                    Contests");
        System.out.println("              Created by PlayGames_");
        System.out.println("-------------------------------------------------");

        this.contestActive = false;

        Main.SystemOut("Checking Config");
        config = getConfig();
        File configFile = new File(getDataFolder(), "config.yml");
        configFile.mkdir();

        if (!config.contains("contestTime")) {
            config.set("contestTime", 120);
            Main.SystemOut("Added 'contestTime' to the config!");
        }
        if (!config.contains("contestQuestions")) {
            ArrayList<ChatContest> contQ = new ArrayList<ChatContest>();
            ChatContest Contest1 = new ChatContest("What is 3+3?", "6", 371, 1, false);
            ChatContest Contest2 = new ChatContest("What is 5*5?", "25", 371, 2, false);
            ChatContest Contest3 = new ChatContest("What are the first 3 digits of Pi", "3.14", 266, 1, false);
            ChatContest Contest4 = new ChatContest("What is 2+2", "4", 0, 100, true);
            contQ.add(Contest1);
            contQ.add(Contest2);
            contQ.add(Contest3);
            contQ.add(Contest4);

            int qNo = 0;
            for (ChatContest cont : contQ) {
                String question = cont.getQuestion();
                String answer = cont.getAnswer();
                int prize = cont.getPrize();
                int amount = cont.getAmount();
                boolean economy = cont.getEconomy();

                qNo++;
                config.set("contestQuestions." + qNo + ".question", question);
                config.set("contestQuestions." + qNo + ".answer", answer);
                config.set("contestQuestions." + qNo + ".prize", prize);
                config.set("contestQuestions." + qNo + ".amount", amount);
                config.set("contestQuestions." + qNo + ".economy", economy);
            }

            Main.SystemOut("Added 'contestQuestions' to the config!");
        }
        saveConfig();
        Main.SystemOut("Finished Checking Config");

        Main.SystemOut("Loading all Questions from the Config!");

        for (String key : config.getConfigurationSection("contestQuestions").getKeys(false)) {
            ChatContest Contest = new ChatContest(config.getString("contestQuestions." + key + ".question"), config.getString("contestQuestions." + key + ".answer"), config.getInt("contestQuestions." + key + ".prize"), config.getInt("contestQuestions." + key + ".amount"), config.getBoolean("contestQuestions." + key + ".economy"));
            Contests c = new Contests();
            c.registerContest(Contest);
        }

        Main.SystemOut("Loaded " + this.contestQandA.size() + " Questions with their answers!");

        Main.SystemOut("Registering Events");
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
        Main.SystemOut("Finished Registering Events");

        Main.SystemOut("Attempting to send Stats");
        try {
            MetricsLite metrics = new MetricsLite(this);
            metrics.start();
            Main.SystemOut("Stats are now sending!");
        } catch (IOException e) {
            // Failed to submit the stats :-(
            Main.SystemOut("Stats failed to send");
        }

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                startContest();
            }
        }, 20, 20 * this.config.getInt("contestTime"));
    }

    @Override
    public void onDisable() {
    }

    //Utility
    public static void SystemOut(String s) {
        java.util.Date time = new Date();
        SimpleDateFormat date2 = new SimpleDateFormat("HH:mm:ss");
        System.out.print("[Contests] " + "[" + date2.format(time) + "] " + s);
    }

    public void sendPMsg(Player p, String s) {
        p.sendMessage("§f[§bContests§f] §r" + s);
    }

    public void broadcastMsg(String s) {
        Bukkit.broadcastMessage("§f[§bContests§f] §r" + s);
    }

    //Start The Contest
    public final void startContest() {
        if (this.contestActive) {
        } else {
            this.contestAmount++;
            this.contestActive = true;

            Random ran = new Random();
            int toChoose = ran.nextInt(Main.contestQandA.size());

            this.broadcastMsg("§6A new contest has started! The question is §f" + Main.contestQandA.get(toChoose).getQuestion() + "§6 - To answer the question and win a prize, just type in chat '§bA: {The Answer}'§6! Have fun");
            this.currentContest = Main.contestQandA.get(toChoose);
        }
    }

    //Async Player Chat
    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        if (event.getMessage().startsWith("A: ") || event.getMessage().startsWith("a: ") || event.getMessage().startsWith(" A: ") || event.getMessage().startsWith(" a: ")) {
            if (event.getPlayer().hasPermission("contests.use")) {
                if (this.contestActive) {
                    event.setCancelled(true);
                    if (event.getMessage().toLowerCase().contains(currentContest.getAnswer().toLowerCase())) {
                        this.sendPMsg(event.getPlayer(), "§cCorrect!");
                        Firework fw = (Firework) event.getPlayer().getWorld().spawnEntity(event.getPlayer().getLocation(), EntityType.FIREWORK);
                        FireworkMeta fwm = fw.getFireworkMeta();
                        FireworkEffect effect = FireworkEffect.builder().trail(true).withColor(Color.AQUA).with(FireworkEffect.Type.BURST).build();
                        fwm.addEffect(effect);
                        fwm.setPower((int) 0.1);
                        fw.setFireworkMeta(fwm);
                        this.contestActive = false;
                        this.broadcastMsg("§6The winner of the competition is§3 " + event.getPlayer().getName() + "§6!");
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ANVIL_LAND, 10, 10);
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.LEVEL_UP, 1, 1);
                        this.currentContest.getPrize();
                        event.getPlayer().getInventory().addItem(new ItemStack(this.currentContest.getPrize(), this.currentContest.getAmount()));
                    } else {
                        this.sendPMsg(event.getPlayer(), "§cIncorrect!");
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLAZE_DEATH, 10, 10);
                    }
                } else {
                    event.setCancelled(true);
                    this.sendPMsg(event.getPlayer(), "§cNo contest is active at this time!");
                }
            }
        }
    }

    //Commands :)
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player send = (Player) sender;
            if (commandLabel.equalsIgnoreCase("Contests")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("help")) {
                        this.sendPMsg(send, "§6To use §bContests§6 all you need to do is wait for a contest to be announced in the chat and then answer by doing §f'A: {your answer}'§6!");
                        if (send.hasPermission("contests.force")) {
                            this.sendPMsg(send, "§cTo force a Contest, run §f'/Contests force'");
                        }
                        if (send.hasPermission("contests.reload")) {
                            this.sendPMsg(send, "§cTo reload Contests run §f'/Contests reload'");
                        }
                        send.sendMessage(" ");
                    } else if (args[0].equalsIgnoreCase("force")) {
                        if (send.hasPermission("contests.force")) {
                            if (this.contestActive) {
                                this.sendPMsg(send, "A contest is already in progress!");
                            } else {
                                this.sendPMsg(send, "Force started the contest!");
                                this.startContest();
                            }
                        } else {
                            this.sendPMsg(send, "§cUnknown Command! Type '/Contests help' for info");
                        }
                    } else if (args[0].equalsIgnoreCase("reload")) {
                        if (send.hasPermission("contests.reload")) {
                            this.sendPMsg(send, "§6You need to run §f'/reload' §6to reload this plugin, until further notice, sorry :/");
                        } else {
                            this.sendPMsg(send, "§cUnknown Command! Type '/Contests help' for info");
                        }
                    }
                } else {
                    this.sendPMsg(send, "§cUnknown Command! Type '/Contests help' for info");
                }
            }
        } else {
            if (args[0].equalsIgnoreCase("force")) {
                if (this.contestActive) {
                    sender.sendMessage("A contest is already in progress!");
                } else {
                    sender.sendMessage("Force started the contest!");
                    this.startContest();
                    return true;
                }

            }
        }
        return false;
    }
} 