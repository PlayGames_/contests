package net.playgames_.contests;

public class Contests {
    
    public void registerContest(ChatContest contest){
        Main.contestQandA.add(contest);
        Main.SystemOut("Added Question '" + contest.getQuestion() + "' to the question list!");
    }
    
    public void unregisterContest(ChatContest contest){
        if (Main.contestQandA.contains(contest)){
            Main.contestQandA.remove(contest);
        }
    }
    
    public boolean contestIsRegistered(ChatContest contest){
        if (Main.contestQandA.contains(contest)){
            return true;
        }else{
            return false;
        }
    }
    
}
